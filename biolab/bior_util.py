import numpy as np
import math


# Some ancillary functions we will need for pyro
def beta_to_normal(rho):
    inferred_mean = np.divide(rho[0], np.add(rho[0], rho[1]))
    factor = np.divide(rho[1], np.multiply(rho[0], np.add(np.full(len(rho[0]), 1.0), rho[0], rho[1])))
    inferred_std = np.array([inferred_mean[i] * math.sqrt(factor[i]) for i in range(len(rho[0]))])
    return [inferred_mean, inferred_std]


