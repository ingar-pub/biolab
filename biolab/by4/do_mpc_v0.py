sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as plt
from casadi import *
from casadi.tools import *
import pdb
import sys
import do_mpc

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import time

""" User settings: """
show_animation = True
store_results = True

"""
do-mpc model definition
"""
def build_model():
    """
    --------------------------------------------------------------------------
    template_model: Variables / RHS / AUX
    --------------------------------------------------------------------------
    """
    model_type = 'continuous' # either 'discrete' or 'continuous'
    model = do_mpc.model.Model(model_type)

    # Certain parameters
    # theta = [0.5, 0.5, 0.5, 0.5]
    # th0 = 0.5
    # th1 = 0.5
    # th2 = 0.5
    # th3 = 0.5
    # mu_m  = 0.02

    # States struct (optimization variables):
    X0_s = model.set_variable('_x',  'X0_s') # X0_s biomass concentration, g/l. Ref: 1 - 10 g/l.
    X1_s = model.set_variable('_x',  'X1_s') # X1_s substrate concentration, g/l. Ref: 0.1 g/l. Initial 0.1

    # Input struct (optimization variables):
    u0 = model.set_variable('_u',  'u0') # inp1: dilution factor h-1. Ref: 0.05 - 0.20 h-10.
    u1 = model.set_variable('_u',  'u1') # inp2: substrate concentration in the feed g/l. Ref: 5 - 35 g/l

    # Fixed parameters:
    # Y_x = model.set_variable('_p',  'Y_x')
    # S_in = model.set_variable('_p', 'S_in')

    # Uncertain parameters:
    th0 = model.set_variable('_p', 'th0')
    th1 = model.set_variable('_p', 'th1')
    th2 = model.set_variable('_p', 'th2')
    th3 = model.set_variable('_p', 'th3')

    # Auxiliary term
    r = th0 * X1_s / (th1 + X1_s)

    # Differential equations
    model.set_rhs('X0_s', (r - u0 - th3) * X0_s)
    model.set_rhs('X1_s', (r * X0_s / th2) + u0 * (u1 - X1_s))

    # Build the model
    model.setup()

    return model

"""
do-mpc controller mpc definition
"""
def build_mpc(model):
    """
    --------------------------------------------------------------------------
    template_mpc: tuning parameters
    --------------------------------------------------------------------------
    """
    mpc = do_mpc.controller.MPC(model)

    setup_mpc = {
        'n_horizon': 20,
        'n_robust': 1,
        'open_loop': 0,
        't_step': 1.0,
        'state_discretization': 'collocation',
        'collocation_type': 'radau',
        'collocation_deg': 2,
        'collocation_ni': 2,
        'store_full_solution': True,
        # Use MA27 linear solver in ipopt for faster calculations:
        #'nlpsol_opts': {'ipopt.linear_solver': 'MA27'}
    }

    mpc.set_param(**setup_mpc)

    mterm = -model.x['X0_s'] # stage cost
    lterm = -model.x['X0_s'] # terminal cost

    mpc.set_objective(mterm=mterm, lterm=lterm)
    mpc.set_rterm(u0=1.0, u1=1.0) # penalty on input changes

    # Constraints
    # lower bounds of the states
    mpc.bounds['lower', '_x', 'X0_s'] = 0.0
    mpc.bounds['lower', '_x', 'X1_s'] = 0.0

    # upper bounds of the states
    # mpc.bounds['upper', '_x','X0_s'] = 3.7
    # mpc.bounds['upper', '_x','X1_s'] = 3.0

    # upper and lower bounds of the control input
    mpc.bounds['lower','_u','u0'] = 0.05
    mpc.bounds['upper','_u','u0'] = 0.20
    mpc.bounds['lower','_u','u1'] = 5
    mpc.bounds['upper','_u','u1'] = 35

    # Uncertain values
    th0_values = np.array([0.5, 0.4, 0.3])
    th1_values = np.array([0.5, 0.4, 0.3])
    th2_values = np.array([0.5, 0.4, 0.3])
    th3_values = np.array([0.5, 0.4, 0.3])

    mpc.set_uncertainty_values(
        th0 = th0_values, 
        th1 = th1_values, 
        th2 = th2_values, 
        th3 = th3_values
    )

    mpc.setup()

    return mpc

"""
do-mpc simulator mpc definition
"""
def build_simulator(model):
    """
    --------------------------------------------------------------------------
    template_simulator: tuning parameters
    --------------------------------------------------------------------------
    """
    simulator = do_mpc.simulator.Simulator(model)

    params_simulator = {
        'integration_tool': 'cvodes',
        'abstol': 1e-10,
        'reltol': 1e-10,
        't_step': 1.0
    }

    simulator.set_param(**params_simulator)

    # Realizations of uncertain parameters
    p_num = simulator.get_p_template()
    p_num['th0'] = 0.5
    p_num['th1'] = 0.5
    p_num['th2'] = 0.5
    p_num['th3'] = 0.5

    def p_fun(t_now):
        return p_num

    simulator.set_p_fun(p_fun)

    simulator.setup()

    return simulator

"""
Get configured do-mpc modules
"""
model = build_model()
mpc = build_mpc(model)
simulator = build_simulator(model)
estimator = do_mpc.estimator.StateFeedback(model)

"""
Set initial state
"""
X0_s_0 = 5.5 # Concentration biomass [g/l]
X1_s_0 = 0.1 # Concentration substrate [g/l]

x0 = np.array([X0_s_0, X1_s_0])

# Set for controller, simulator and estimator
mpc.x0 = x0
simulator.x0 = x0
estimator.x0 = x0

mpc.set_initial_guess()

"""
Setup graphic:
"""

fig, ax, graphics = do_mpc.graphics.default_plot(mpc.data, figsize=(8,5))
plt.ion()

"""
Run MPC main loop:
"""

for k in range(50):
    u0 = mpc.make_step(x0)
    y_next = simulator.make_step(u0)
    x0 = estimator.make_step(y_next)


    if show_animation:
        graphics.plot_results(t_ind=k)
        graphics.plot_predictions(t_ind=k)
        graphics.reset_axes()
        plt.show()
        plt.pause(0.01)

input('Press any key to exit.')

# Store results:
if store_results:
    do_mpc.data.save_results([mpc, simulator], result_name='do_mpc_v0_results', result_path='./biolab/by4/')
