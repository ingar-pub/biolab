sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as plt
from casadi import *
from casadi.tools import *
import pdb
import sys
import do_mpc

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import time

""" Load results """
results = do_mpc.data.load_results('./biolab/by4/do_mpc_v0_results.pkl')

print(results['mpc'])
print(results['simulator'])

x = results['mpc']['_x']
print(x.shape)
print(x)

u = results['mpc']['_u']
print(u)

p = results['mpc']['_p']
print(p.shape)
print(p)

