# Biolab

Reinforcemente learning in python applied to bioproccess.

## Desarrollo:

### Instalar Anaconda Python:

- Instalar conda desde aquí: https://www.anaconda.com/products/individual

- Crear conda environment (Linux: bash, Windows: Anaconda Prompt):

```bash
conda create -n biolab python=3.8
conda activate biolab
```

### Instalar dependencias:

```bash
pip install --force-reinstall -r requirements.txt
```

### IDE: Visual Studio Code

- Download e instalar: https://code.visualstudio.com/Download
    
- Iniciar en el directorio de proyecto: 
```
code .
```

- Para que los imports internos funcionen, tiene que estar seteado PYTHONPATH en el terminal
```
{
    "python.pythonPath": "/home/fede/anaconda3/envs/biolab/bin/python",
    ...
    "terminal.integrated.env.linux": {
        "PYTHONPATH":"."
    }
}
```

### Run in Binder

- Iniciar repositorio en Binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ingar-pub%2Fbiolab/master)

- Iniciar do_mpc simulation baker yeast 4 parameters (version 0) en Binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ingar-pub%2Fbiolab/master?filepath=%2Fbiolab%2Fby4%2Fdo_mpc_v0.ipynb)

- Iniciar svi simulation baker yeast 4 parameters (version 0) en Binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ingar-pub%2Fbiolab/master?filepath=%2Fbiolab%2Fby4%2Fpyro_v0.ipynb)
